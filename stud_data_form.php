<?php 

require('db_config.php');
		
		$id = $_GET['id'];
		$sql = "SELECT * FROM tbl_student WHERE stud_id=:id";
	    $stat = $conn->prepare($sql);
	    $stat->execute([':id'=>$id]);
	    $data = $stat->fetch(PDO::FETCH_OBJ);	

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data1 = $stat->fetchall(PDO::FETCH_OBJ);

	    $sql = "SELECT * FROM tbl_school";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data2 = $stat->fetchall(PDO::FETCH_OBJ);
include ('header.php'); ?>

<div>
	<nav class="navbar navar-default">
		<div class="container-fluid">
			<div class="navbar-header">
			</div>

			<form action="" method="post"  enctype="multipart/form-data" style="margin-top: 25px;">
				<!-- <div class="row">
					<div class="col-md-12"></div>
				</div> -->
				<div class="row">
					<!-- 	<div class="col-md-3"></div> -->
					<div class="text-center">
					<label style="margin-bottom: 25px; font-size: 25px; color: #0000FF"> STUDENT DATA </label>
					</div>
						<div class="col-md-1"></div>
						<div class="col-md-2">
						 	<p><img src="uploads/<?= $data->stud_profile; ?>" height="175" width="150" /></p>
						</div>
						<div class="col-md-8" style="margin-left: 5px;">
							
							<div>
								<label>Last Name:</label> <?= $data->last_name; ?>
								<label >First Name:</label> <?= $data->first_name; ?>
								<label>Middle Name:</label> <?php if($data->mi_name == null){
									echo "NONE";}else{ echo $data->mi_name;}?>
							</div>
							<div>
								<label>Age:</label> <?= $data->age; ?>
								<label>Address:</label> <?= $data->stud_address; ?>
							</div>
							<div>
								<label>Course:</label>  <?= $data->course; ?>
								<label>School:</label> <?php foreach ($data2 as $val2):
									if($val2->scol_id ==  $data->scol_id){
									echo $val2->scol_name; } endforeach;?>
							</div>
							<div>
								<label>Skills:</label> <?= $data->skills; ?>
							</div>
							<div>
								<label>Contact Number:</label> <?= $data->stud_contact; ?>
								<label >Email:</label> <?= $data->stud_email; ?>
							</div>
							<div>
								<label>Objectives:</label>
								<label  style="margin-left: 10em;">Company:</label> <?php foreach ($data1 as $val1):
									if($val1->comp_id ==  $data->comp_id){
									echo $val1->comp_name; } endforeach;?>
								<div style="margin-right: 150px; margin-left: 50px;">
										 <p><?= $data->stud_obj; ?></p>
								</div>
							<div>
								<label style="color: grey;"><h5><b>REFERENCE</b></h5></label>
								<div>
								<label>Full Name:</label> <?= $data->ref_name; ?>
								<label >Contact Number:</label> <?= $data->ref_contact; ?>
								</div>
							</div>

							<div style="margin-left: 15em; margin-right: 13em; margin-top: 20px;">
								<p class="pull-right">
										<a class="btn btn-warning" href="stud_edit1.php?id=<?= $data->stud_id; ?>">Edit</a>
										<a class="btn btn-danger" href="stud_delete.php?id=<?= $data->stud_id; ?>">Delete</a></p></a>
							</div>
							
						</div>
				</div>
			</form>
		</div>
	</nav>
</div>
<?php include('footer.php');?>