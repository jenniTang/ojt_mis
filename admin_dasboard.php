<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lifetime.com</title>
  <link rel="icon" href="gallery/lifetime.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <!-- datatables -->
      <link rel="stylesheet" href="css/jquery.dataTables.min.css">
      <script src="js/jquery-1.12.4.js"></script>
      <script src="js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable( {
              "scrollY":        "375px",
              "scrollCollapse": true,
              "paging":         true
          } );
      } );
      </script>
 
</head>
<body class="hold-transition skin-green sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <nav class="navbar navbar-static-top">
      
      <span><a href="#" class="sidebar-toggle" role="button"></a></span>
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <li>
              <a href="#" data-toggle="control-sidebar">Log out <i class="fa fa-user"></i></a>
            </li>
          </ul>
        </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div>
            <img src="gallery/lifetime.png" height="200" width="200">
          </div>
        </div>

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">

         <li class="header text-center">MANAGEMENT</li>
              <li><a href="#"><i class="fa fa-circle-o text-green"></i> <span>Users</span></a></li>
              <li><a href="stud_data.php"><i class="fa fa-circle-o text-aqua"></i> <span>Students</span></a></li>
              <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Schools</span></a></li>
              <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Companies</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
  </aside>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    
        <!-- Content Header (Page header) -->
       <!--  <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          -->