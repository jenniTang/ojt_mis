<?php

session_start();

require 'db_config.php';

   if(isset($_POST["login"]))  
      {  
           if(empty($_POST["username"]) || empty($_POST["password"]))  
           {  
                $errMsg = 'All fields are required';  
           }  
           else  
           {  
                $query = "SELECT * FROM tbl_user WHERE username = :username AND password = :password";  
                $statement = $conn->prepare($query);  
                $statement->execute(  
                     array(  
                          'username'     =>     $_POST["username"],  
                          'password'     =>     $_POST["password"]  
                     )  
                );  
                $count = $statement->rowCount();  
                $fetch = $statement->fetch(PDO::FETCH_OBJ);  

                if($count > 0)  
                {  
                  if($_POST['username'] && $_POST['password'] == 'admin') {
                     $_SESSION["id"] =  $fetch->user_id;
                     $_SESSION["username"] = $_POST["username"];   
                     header("location: stud_data.php");

               
                   }
                   else{
                     $_SESSION["id"] =  $fetch->user_id;
                     $_SESSION["username"] = $_POST["username"];   
                     header("location: stud_data.php");
                  }
                }  
                else  
                {  
                     $errMsg = 'Wrong Data';  
                } 

           }  
      }  
 
 ?>  
<?php
				if(isset($errMsg)){
					echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$errMsg.'</div>';
				}
			?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Lifetime.com</title>
  <link rel="icon" href="gallery/lifetime.png">
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
      <!-- datatables -->
      <link rel="stylesheet" href="css/jquery.dataTables.min.css">
      <script src="js/jquery-1.12.4.js"></script>
      <script src="js/jquery.dataTables.min.js"></script>
      <script type="text/javascript">
      $(document).ready(function() {
          $('#example').DataTable( {
              "scrollY":        "375px",
              "scrollCollapse": true,
              "paging":         false
          } );
      } );
      </script>
 
</head>
<style>
  #def{}
  .txt{
    width: 250px;
    margin-left: 30px;
    margin-bottom: 2px;

  }
  .txt label{
    font-size: 17px;
    margin-top: 15px;
    margin-left: 0px;
  }
  .pad{
    height: 250px;
    width: 400px;
    margin-left: 30em;
    margin-top: 10em;
    border:2px dashed grey;
  }
  .btn2{
    width: 100px;
    height: 35px;
    border-radius: 3px;
    border:1px solid grey;
  }

</style>
<body class="" style="background-color: #CCFFFF;">

<section class="content-header">
  <div class="panel pad" id="def">
  <div class="panel-body txt" id="def">
    <form action="" method="post">
      <label>Username:</label>
      <div>
        <input class="txt" id="def" type="text" name="username" placeholder="Enter Username">
      </div>
      <label>Password:</label>
      <div>
        <input class="txt" id="def" type="password" name="password" placeholder="Enter Password">
      </div>
      <p>Don't have account?  <a href="#">Sign Up!</a></p>
      <span style="margin-left: 15.5em;"><input class="btn2 btn-success" type="submit" name="login" value="Login"></span>
  	</form>
  </div>
</div>
</section>