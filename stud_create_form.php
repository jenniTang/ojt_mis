<?php 

require_once('db_config.php');

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data1 = $stat->fetchall(PDO::FETCH_OBJ);

	    $sql = "SELECT * FROM tbl_school";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data2 = $stat->fetchall(PDO::FETCH_OBJ);

 include ('header.php'); ?>
<style>
	#default{
			
	}
	.long{ 
		width: 500px;
	}
	.age{
		width: 35px;
	}
	.name{
		width: 125px;
	}
	.info{
		width: 200px;
	}
	.note{
		height: 100px;
		width: 500px;
	}
	.box{
		height: 50px;
	}
</style>
 <section class="content-header">
          <h1>
            Note:
            <small>The Data should be upon the real information of the student.</small>
          </h1>
	
	<form action="stud_create.php" method="post"  enctype="multipart/form-data" style="margin-top: 25px;">
		<!-- <div class="row">
			<div class="col-md-12"></div>
		</div> -->
		<div class="row">
			<!-- 	<div class="col-md-3"></div> -->
			<div class="text-center">
			<label style="margin-bottom: 25px; font-size: 25px; color: #0000FF"> CREATE NEW STUDENT DATA </label>
			</div>

				<div class="col-md-2">
					<label><b>Profile</b></label>
				 	<p><img src="gallery/user.png" height="175" width="150" /></p>
						<input class="input-group" type="file" name="profile" accept="image/*" />
				</div>
				<div class="col-md-9" style="margin-left: 5px;">
					
					<div>
						<label>Last Name: </label>
							<input class="name" id="default" type="text" name="lname" placeholder="Last Name">
						<label >First Name: </label>
							<input class="name" id="default" type="text" name="fname" placeholder="First Name">
						<label>Middle Name: </label>
							<input class="name" id="default" type="text" name="mname" placeholder="Middle Name">
					</div>
					<div>
						<label>Age: </label>
							<input class="age" id="default" type="text" name="age" placeholder="Age">
						<label>Address: </label>
							<input class="long" id="default" type="text" name="address" id="address" placeholder=" Barangay | Municipality | Province">
					</div>
					<div>
						<label>Course: </label>
						<select name="course">
							<option>choose bachelor</option>
							<option>BS Computer Science</option>
							<option>BS Computer Engineering</option>
							<option>BS Information System</option>
							<option>BS Industrial Technology</option>
							<option>BS Information Technology </option>
						</select>
						<label>School</label>
						<select name="scol_name">
							<option>school studied</option>
							<?php foreach($data2 as $value):?>
							<option><?= $value->scol_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div>
						<label>Skills: </label>
							<input class="long" id="default" type="text" name="skills" id="address" placeholder="">
					</div>
					<div>
						<label>Contact Number: </label>
							<input class="info" id="default" type="text" name="cnum" placeholder="Contact Number">
						<label >Email: </label>
							<input class="info" id="default" type="text" name="email" placeholder="Email">
					</div>
					<div>
						<label>Objectives:</label>
						<label style="margin-left: 15em;">Company Name:</label>
						<select name="comp_name">
							<option>choose company</option>
							<?php foreach($data1 as $value):?>
							<option><?= $value->comp_name; ?></option>
							<?php endforeach; ?>
						</select>
						<div style="margin-right: 150px; margin-left: 50px;">
								<textarea name="obj" type="text" id="default" cols="30" rows="7" class="box"  placeholder="Type atleast one sentence." ></textarea>
						</div>
					<div>
						<label style="color: grey;"><h5><b>REFERENCE</b></h5></label>
						<div>
						<label>Full Name:</label>
							<input class="info" id="default" type="text" name="ref_name" placeholder="Full Name">
						<label >Contact Number: </label>
							<input class="info" id="default" type="text" name="ref_cnum" placeholder="Contact Number">
						</div>
					</div>

					<div style="margin-left: 28em; margin-right: 5em; margin-top: 20px;">
						<input  class="btn btn-primary box" id="default" type="submit" name="add" value="Add Student">
					</div>
					
				</div>
		</div>
	</form>

<?php include('footer.php');?>