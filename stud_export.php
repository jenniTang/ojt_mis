<?php
include('db_config.php');

if(isset($_POST["export"])){
		 header('Content-Type: text/csv; charset=utf-8');  
			header('Content-Disposition: attachment; filename=data.csv');
			      // require 'connection.php';
			      $output = fopen("php://output", "w");  
			      fputcsv($output, array('stud_id', 'first_name','mi_name', 'last_name','age','stud_address','course','scol_id','stud_contact','stud_email'));  
			     
			      $sql = "SELECT stud_id, first_name, mi_name, last_name, age, stud_address, course, scol_id, stud_contact, stud_email from tbl_student ORDER BY stud_id ASC";  
			
					$stat = $conn->prepare($sql);
					$stat->execute();
					$list = $stat->fetchall(PDO::FETCH_ASSOC);
					
						foreach ($list as $key => $data) :
							fputcsv($output, $data);
						endforeach;
			      // while($row = mysqli_fetch_assoc($result))  
			      // {  
			      //      fputcsv($output, $row);  
			      // } 
			      fclose($output);  
				 } 
?>