<?php 
include('db_config.php');
		
		$sql = "SELECT * FROM tbl_student";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data = $stat->fetchall(PDO::FETCH_OBJ);

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data1 = $stat->fetchall(PDO::FETCH_OBJ);

	    $sql = "SELECT * FROM tbl_school";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data2 = $stat->fetchall(PDO::FETCH_OBJ);

include ('header.php'); ?>
<style>

	table{
		border: 0px solid black;
	}
	table tr{
		border: 1px solid black;
	}
	table tr th{
		text-align: center;
		color: white;
		background-color: #003399;
	}
	table tr td{
		border:1px solid black;
		background-color: #CCFFCC;
	}
	table tr td label{
		font-size: 12px;
		color: #444;
		margin-left: 25px;
	}

	#tbl.mon{
		border:  1px solid grey;
		background-color: #FFFF66;
		font-size: 17px;
		color: #444;
		text-align: center;
	}
	.mon2{
		border: 1px solid grey;
		/*font-size: 10px;
		color: #444;*/
		background-color: #FFFF99;
		text-align: left;
	}
	.mon2 label{
		font-size: 15px;
		color: #444;
		margin-top:-5px;
	}
	.box{
		width: 350px;
	}

</style>


	<div>
		<nav class="navbar navar-default">
			<div class="container-fluid">
				<div class="navbar-header">
				</div>
				
				<div class="row" style="margin-top: 50px;">
				
				<div class="col-md-12">
				
				<form action="stud_export.php" method="post">
					<table id="example" class="display" style="width:100%">
						 <thead>
				            <tr>
				            	<th>ID</th>
				                <th>FULL NAME</th>
				                <th>AGE</th>
				                <th>ADDRESS</th>
				                <th>COURSE</th>
				                <th>SCHOOL</th>
				                <th>CONTACT</th>
				            </tr>
				        </thead>
				        <tbody class="datashow">
						<?php foreach($data as $val): ?>
							<tr>			
							<!-- 	<td><a class="btn" href="">
					       		 	<p><img src="uploads/<?= $val->stud_profile; ?>" height="150" width="100" /></p></a>
					       		</td> -->
					       		<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?= $val->stud_id; ?></label></a>
					       		</td>
					       		<td><a href="stud_data_form.php?id=<?=$val->stud_id?>">
									<label><?= $val->first_name; ?> <?= $val->last_name; ?> <?= $val->mi_name; ?>
									</label></a>
								</td>
								<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?= $val->age; ?></label></a>
								</td>
								<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?= $val->stud_address; ?></label></a>
								</td>
								<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?= $val->course; ?></label></a>
								</td>
								<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?php foreach ($data2 as $val2):
									if($val2->scol_id ==  $val->scol_id){
									echo $val2->scol_name; } endforeach;?></label></a>
								</td>
								<td><a href="stud_data_form.php?id=<?=$val->stud_id?>"><label><?= $val->stud_contact; ?> | <?= $val->stud_email; ?></label></a>
								</td>
							</tr>
						
						<?php endforeach; ?>
						</tbody>
				        <tfoot>
				            <tr>
				                <th></th>
				                <th></th>
				                <th></th>
				                <th></th>
				                <th></th>
				                <th></th>
				                <th></th>
				            </tr>
				        </tfoot>
					</table>
					<div class="pull-right">
						<input style="margin-top: 1em;" class="btn btn-success" type="submit" value="Export Data" name="export">
					</div>
				</form>
		
				</div>
				</div>
		</nav>
	</div>
<?php include('footer.php');?>