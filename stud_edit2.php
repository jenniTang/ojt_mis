<?php 

require('db_config.php');
		
		$id = $_GET['id'];
		$sql = "SELECT * FROM tbl_student WHERE stud_id=:id";
	    $stat = $conn->prepare($sql);
	    $stat->execute([':id'=>$id]);
	    $data = $stat->fetch(PDO::FETCH_OBJ);	

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data1 = $stat->fetchall(PDO::FETCH_OBJ);

	    $sql = "SELECT * FROM tbl_school";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data2 = $stat->fetchall(PDO::FETCH_OBJ);

    if(isset($_POST['update'])){

	    $lname = $_POST['lname'];
	    $fname = $_POST['fname'];
	    $mname = $_POST['mname'];
	    $age = $_POST['age'];
	    $address = $_POST['address'];
		$course = $_POST['course'];
		$skills = $_POST['skills'];
		$cnum = $_POST['cnum'];
		$email =$_POST['email'];
		$obj = $_POST['obj'];
		$ref_name = $_POST['ref_name'];
		$ref_cnum = $_POST['ref_cnum'];

		$comp_id = $_POST['comp_id'];
		$scol_id = $_POST['scol_id'];
			// $scol_name = $_POST['scol_name'];
			// $sql = "SELECT * FROM tbl_school WHERE scol_name=:scol_name";
			// $stat = $conn->prepare($sql);
			// $stat->execute(['scol_name' => $scol_name]);
			// $data3 = $stat->fetch(PDO::FETCH_OBJ);
			// $scol_id = $data3->scol_id;

			// $comp_name = $_POST['comp_name'];
			// $sql = "SELECT * FROM tbl_company WHERE comp_name=:comp_name";
			// $stat = $conn->prepare($sql);
			// $stat->execute(['comp_name' => $comp_name]);
			// $data4 = $stat->fetch(PDO::FETCH_OBJ);
			// $comp_id = $data4->comp_id;

		$imgFile = $_FILES['profile']['name'];
		$tmp_dir = $_FILES['profile']['tmp_name'];
		$imgSize = $_FILES['profile']['size'];
			if(empty($lname||$fname)){
				$errMSG = "Please Enter Username.";
			}
			else if(empty($cnum||$email)){
				$errMSG = "Please Enter Your Complete Contact.";
			} 
			elseif (empty($age || $address || $course || $skills || $obj || $ref_name || $ref_cnum || $scol_id || $comp_id )) {
				$errMSG = "Please Fill Up Everything";
			}
			// else if(empty($imgFile)){
			// 	$errMSG = "Please Select Image File.";
			// }
			else
			{
				if($imgFile){
					$upload_dir = 'uploads/';
					$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
					$valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
					$profile = rand(1000,1000000).".".$imgExt;
					if(in_array($imgExt, $valid_extensions)){
						if($imgSize < 5000000){
							unlink($upload_dir.$edit_row['profile']);
							move_uploaded_file($tmp_dir,$upload_dir.$profile);
						}
						else{
							$errMSG = "Sorry, Your File Is Too Large To Upload. It Should Be Less Than 5MB.";
							echo "<br>" . $errMSG;
						}
					}
					else{
						$errMSG = "Sorry, only JPG, JPEG, PNG & GIF Extension Files Are Allowed.";
						echo "<br>" . $errMSG;		
					}
				}
				else
				{
					$profile = $edit_row['profile'];
				}
			
			if(!isset($errMSG))
			{
			
			$sql = 'UPDATE tbl_student SET last_name=:lname, first_name=:fname, mi_name=:mname, age=:age, stud_address=:address, course=:course, scol_id=:scol_id, skills=:skills, stud_contact=:cnum, stud_email=:email, stud_obj=:obj, ref_name=:ref_name, ref_contact=:ref_cnum, comp_id=:comp_id, stud_profile=:profile WHERE stud_id=:id';
		    $stat = $conn->prepare($sql);
		    $stat->execute([':lname' => $lname,
				    		':fname' => $fname,
				    		':mname' => $mname,
				    		':age' => $age,
				    		':address' => $address,
				    		':course' => $course,
				    		':scol_id' => $scol_id,
				    		':skills' => $skills,
				    		':cnum' => $cnum,
				    		':email' => $email,
				    		':obj' => $obj,
				    		':ref_name' => $ref_name,
				    		':ref_cnum' => $ref_cnum,
				    		':comp_id' => $comp_id,
				    		':profile' => $profile,
				    		':id' => $id]);

		   		header("Location: stud_data.php");
		   	}
		}
		echo "Nothing Happens?";
		echo "Well . . . ".$errMSG;
		}

			

 include ('header.php'); ?>
<style>
	#default{
			
	}
	.long{ 
		width: 500px;
	}
	.age{
		width: 35px;
	}
	.name{
		width: 125px;
	}
	.info{
		width: 200px;
	}
	.note{
		height: 100px;
		width: 500px;
	}
	.box{
		height: 50px;
	}
</style>
 <section class="content-header">
          <h1>
            Note:
            <small>The Data should be upon the real information of the student.</small>
          </h1>
	
	<form action="" method="post"  enctype="multipart/form-data" style="margin-top: 25px;">
		<!-- <div class="row">
			<div class="col-md-12"></div>
		</div> -->
		<div class="row">
			<!-- 	<div class="col-md-3"></div> -->
			<div class="text-center">
			<label style="margin-bottom: 25px; font-size: 25px; color: #0000FF"> STUDENT DATA </label>
			</div>

				<div class="col-md-2">
				 	<p><img src="uploads/<?= $data->stud_profile; ?>" height="175" width="150" /></p>
						<input class="input-group" type="file" name="profile" accept="image/*" />
				</div>
				<div class="col-md-9" style="margin-left: 5px;">
					
					<div>
						<label>Last Name: </label>
							<input class="name" id="default" type="text" name="lname" value="<?= $data->last_name; ?>">
						<label >First Name: </label>
							<input class="name" id="default" type="text" name="fname" value="<?= $data->first_name; ?>" placeholder="First Name">
						<label>Middle Name: </label>
							<input class="name" id="default" type="text" name="mname" value="<?= $data->mi_name; ?>" placeholder="Middle Name">
					</div>
					<div>
						<label>Age: </label>
							<input class="age" id="default" type="text" name="age" value="<?= $data->age; ?>" placeholder="Age">
						<label>Address: </label>
							<input class="long" id="default" type="text" name="address" id="address" value="<?= $data->stud_address; ?>" placeholder=" Barangay | Municipality | Province">
					</div>
					<div>
						<label>Course: </label>
						<select name="course">
							<option><?= $data->course; ?></option>
							<option>BS Computer Science</option>
							<option>BS Computer Engineering</option>
							<option>BS Information System</option>
							<option>BS Industrial Technology</option>
							<option>BS Information Technology </option>
						</select>
						<label for="scol_id">School</label>
						<select name="scol_id">
							<option>school studied</option>
							<?php foreach($data2 as $value):?>
							<option  <?=($value->scol_id ==  $data->scol_id)?'selected':''; ?> value="<?= $value->scol_id; ?>" >
								<?= $value->scol_name; ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div>
						<label>Skills: </label>
							<input class="long" id="default" type="text" name="skills" id="address" value="<?= $data->skills; ?>">
					</div>
					<div>
						<label>Contact Number: </label>
							<input class="info" id="default" type="text" name="cnum" value="<?= $data->stud_contact; ?>" placeholder="Contact Number">
						<label >Email: </label>
							<input class="info" id="default" type="text" name="email" value="<?= $data->stud_email; ?>" placeholder="Email">
					</div>
					<div>
						<label>Objectives:</label>
						<label for="comp_id" style="margin-left: 15em;">Company Name</label>
						<select name="comp_id">
							<option>choose company</option>
							<?php foreach($data1 as $value):?>
							<option <?=($value->comp_id == $data->comp_id)? 'selected':''; ?> value="<?= $value->comp_id; ?>">
								<?= $value->comp_name; ?></option>
							<?php endforeach; ?>
						</select>
						<div style="margin-right: 150px; margin-left: 50px;">
								<textarea name="obj" type="text" id="default" cols="30" rows="7" class="box" placeholder="Type atleast one sentence." ><?= $data->stud_obj; ?></textarea>
						</div>
					<div>
						<label style="color: grey;"><h5><b>REFERENCE</b></h5></label>
						<div>
						<label>Full Name:</label>
							<input class="info" id="default" type="text" name="ref_name" value="<?= $data->ref_name; ?>" placeholder="Full Name">
						<label >Contact Number: </label>
							<input class="info" id="default" type="text" name="ref_cnum" value="<?= $data->ref_contact; ?>" placeholder="Contact Number">
						</div>
					</div>

					<div style="margin-left: 28em; margin-right: 5em; margin-top: 20px;">
						<input  class="btn btn-primary active box" id="default" type="submit" name="update" value="Update Data">
					</div>
					
				</div>
		</div>
	</form>

<?php include('footer.php');?>