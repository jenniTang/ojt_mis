-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2018 at 07:39 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ojt_mis`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_company`
--

CREATE TABLE `tbl_company` (
  `comp_id` int(10) NOT NULL,
  `comp_name` varchar(150) NOT NULL,
  `comp_address` varchar(100) NOT NULL,
  `comp_details` varchar(250) NOT NULL,
  `comp_requirements` varchar(250) NOT NULL,
  `comp_contact` varchar(20) NOT NULL,
  `comp_email` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_company`
--

INSERT INTO `tbl_company` (`comp_id`, `comp_name`, `comp_address`, `comp_details`, `comp_requirements`, `comp_contact`, `comp_email`) VALUES
(1, 'CVISNET', 'Sudlon, Lahug', 'Be Open Share and Serve', 'PHP, Bootstrap, C#, Javascript', '456-765', 'cvisnet.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_school`
--

CREATE TABLE `tbl_school` (
  `scol_id` int(10) NOT NULL,
  `scol_name` varchar(50) NOT NULL,
  `scol_address` varchar(50) NOT NULL,
  `scol_contact` varchar(50) NOT NULL,
  `scol_email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_school`
--

INSERT INTO `tbl_school` (`scol_id`, `scol_name`, `scol_address`, `scol_contact`, `scol_email`) VALUES
(1, 'Naval State University', 'Naval, Biliran', '9876-865', 'nsu@nsu.com'),
(2, 'University of the Philippines', 'UP,Cebu', '23456-456', 'up.com');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student`
--

CREATE TABLE `tbl_student` (
  `stud_id` int(11) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `mi_name` varchar(50) NOT NULL,
  `age` int(2) NOT NULL,
  `stud_address` varchar(250) NOT NULL,
  `course` varchar(250) NOT NULL,
  `scol_id` int(10) NOT NULL,
  `skills` varchar(250) NOT NULL,
  `stud_contact` varchar(20) NOT NULL,
  `stud_email` varchar(150) NOT NULL,
  `stud_obj` text NOT NULL,
  `ref_name` varchar(50) NOT NULL,
  `ref_contact` varchar(20) NOT NULL,
  `comp_id` int(10) NOT NULL,
  `stud_profile` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student`
--

INSERT INTO `tbl_student` (`stud_id`, `last_name`, `first_name`, `mi_name`, `age`, `stud_address`, `course`, `scol_id`, `skills`, `stud_contact`, `stud_email`, `stud_obj`, `ref_name`, `ref_contact`, `comp_id`, `stud_profile`) VALUES
(1, 'Etang', 'Daryl', '', 19, 'Calubian, Leyte', 'BS Information Technology', 2, 'Programming', '23456787654', 'daryl.com', 'To gain more knowledge.', 'Papa', '987453456', 1, '445948.png'),
(2, 'Etang', 'Jennifer', 'Montemor', 20, 'Naval, Biliran', 'BS Computer Engineering', 1, 'Programming', '87654345678', 'jenni@gmail.com', 'To be part of the team =)', 'Mama', '8765434', 1, '152298.jpeg'),
(3, 'Fariol', 'Carl Hentjie', 'Sarili', 22, 'Zamboanga', 'BS Computer Science', 1, 'Office Work', '987654345678', 'fariol@gmail.com', 'Experience', 'Mama', '0987543234567', 1, '338205.jpg'),
(4, 'Sales', 'Pablito Jr.', '', 20, 'Naval, Biliran', 'BS Computer Science', 1, 'Programming', '87654345678', 'pabs@pabs.com', 'Experience', 'Faith Olea', '987654334', 1, '58945.jpg'),
(5, 'Olea', 'Faith Roxanne', 'Delovino', 19, 'Calumpang', 'BS Computer Science', 2, 'Programming', '789876543456', 'olea@olea.com', 'To learn more.', 'Pablito Sales Jr.', '765445678', 1, '799868.jpg'),
(6, 'Denosta', 'Mark Devin', 'Sumayan', 24, 'Mandaue City', 'BS Computer Engineering', 2, 'Programming', '45678765432123456', 'dmarkdevin@gmail.com', 'To see is to believe.', 'jennitang', '23456787654', 1, '891060.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `user_id` int(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `useremail` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`user_id`, `username`, `useremail`, `password`) VALUES
(1, 'jennitang', 'jennitang@gmail.com', 'ewanko'),
(2, 'jennitang', 'jennitang@gmail.com', 'jennitang');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_company`
--
ALTER TABLE `tbl_company`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `tbl_school`
--
ALTER TABLE `tbl_school`
  ADD PRIMARY KEY (`scol_id`);

--
-- Indexes for table `tbl_student`
--
ALTER TABLE `tbl_student`
  ADD PRIMARY KEY (`stud_id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_company`
--
ALTER TABLE `tbl_company`
  MODIFY `comp_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_school`
--
ALTER TABLE `tbl_school`
  MODIFY `scol_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_student`
--
ALTER TABLE `tbl_student`
  MODIFY `stud_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `user_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
