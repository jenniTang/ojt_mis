<?php 
include('db_config.php');
		
		$sql = "SELECT * FROM tbl_student";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data = $stat->fetchall(PDO::FETCH_OBJ);

		$sql = "SELECT * FROM tbl_company";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data1 = $stat->fetchall(PDO::FETCH_OBJ);

	    $sql = "SELECT * FROM tbl_school";
	    $stat = $conn->prepare($sql);
	    $stat->execute();
	    $data2 = $stat->fetchall(PDO::FETCH_OBJ);

include ('header.php'); ?>
<style>

	table{
		border: 0px solid black;
	}
	table tr{
		border: 1px solid black;
	}
	table tr th{
		background-color: #003399;
	}
	table tr td{
	
		background-color: #CCFFCC;
	}
	table tr td label{
		font-size: 16px;
		color: #444;
		margin-left: 25px;
		margin-top: 50px;
	}

	#tbl.mon{
		border:  1px solid grey;
		background-color: #FFFF66;
		font-size: 17px;
		color: #444;
		text-align: center;
	}
	.mon2{
		border: 1px solid grey;
		/*font-size: 10px;
		color: #444;*/
		background-color: #FFFF99;
		text-align: left;
	}
	.mon2 label{
		font-size: 15px;
		color: #444;
		margin-top:-5px;
	}
	.box{
		width: 350px;
	}

</style>


	<div>
		<nav class="navbar navar-default">
			<div class="container-fluid">
				<div class="navbar-header">
				</div>
				
				<div class="row" style="margin-top: 50px;">
				
				<div class="col-md-7">
				
				<form action="" method="get">
					<table id="example" class="display" style="width:100%">
						 <thead>
				            <tr>
				                <th></th>
				                <th></th>
				            </tr>
				        </thead>
				        <tbody class="datashow">
						<?php foreach($data as $val): ?>
							<tr class="btn btn-default">
							
								<td><a class="btn" href="">
					       		 	<p><img src="uploads/<?= $val->stud_profile; ?>" height="150" width="100" /></p></a>
					       		</td>
					       		<td><a href="stud_data_form.php?id=<?=$val->stud_id?>">
									<label style="text-align: left; margin-left: 5px;"><b><?= $val->last_name; ?>, <?= $val->first_name; ?></b><br><?= $val->age;?> yrs old,
										lives in <?= $val->stud_address?>
										<br>a <?= $val->course?> student
									</label>
									<p class="pull-right">
										<a class="btn btn-warning" href="stud_edit1.php?id=<?= $val->stud_id; ?>">Edit</a>
										<a class="btn btn-default" href="stud_delete.php?id=<?= $val->stud_id; ?>">Delete</a></p></a>
								</td>
							</tr>
						
						<?php endforeach; ?>
						</tbody>
				        <tfoot>
				            <tr>
				                <th></th>
				                <th></th>
				            </tr>
				        </tfoot>
					</table>
				</form>
		
				</div>
				<div class="col-md-5">
					<div>
					<table class="mon box" id="tbl">
						<tr>
							<th  class="mon" id="tbl">School Name</th>
							<th  class="mon" id="tbl">Students</th>
						</tr>
						<?php foreach ($data2 as $val2):?>
							<tr >
								<td class="mon2" id="tbl"><label><?= $val2->scol_name; ?></label></td>
								<td  class="mon2" id="tbl"
								<?php $sum = 0;
										foreach ($data as $val):
												if($val->scol_id==$val2->scol_id){
													// $x=1; $sum = $sum + $x;
													$sum += 1;
												}
										endforeach; ?>
									><label><?= $sum; ?></label></td>
							</tr>
						<?php endforeach; ?>
					</table>
					</div>
					<div style="margin-top: 20px;">
					<table class="mon box" id="tbl">
						<tr>
							<th  class="mon" id="tbl">Company Name</th>
							<th  class="mon" id="tbl">Students</th>
						</tr>
						<?php foreach ($data1 as $val1):?>
							<tr >
								<td class="mon2" id="tbl"><label><?= $val1->comp_name; ?></label></td>
								<td  class="mon2" id="tbl"
								<?php $sum = 0;
										foreach ($data as $val):
												if($val->comp_id==$val1->comp_id){
													$x=1; $sum = $sum + $x; }
											endforeach; ?>
									><label><?= $sum; ?></label></td>
							</tr>
						<?php endforeach; ?>
					</table>
					</div>
					<div>
						<a style="margin-top: 14em;" class="btn btn-success" href="stud_data_all.php"> Full Data </a><br />
					</div>
					<div>
					<a style="margin-top: 3px;" class="btn btn-danger" href="stud_create_form.php"> Add New Student </a></div>
				</div>
				</div>
				
			</div>
		</nav>
	</div>
<?php include('footer.php');?>