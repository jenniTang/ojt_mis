<?php 

require_once('db_config.php');


    if(isset($_POST['add'])){

	    $lname = $_POST['lname'];
	    $fname = $_POST['fname'];
	    $mname = $_POST['mname'];
	    $age = $_POST['age'];
	    $address = $_POST['address'];
		$course = $_POST['course'];
		$skills = $_POST['skills'];
		$cnum = $_POST['cnum'];
		$email =$_POST['email'];
		$obj = $_POST['obj'];
		$ref_name = $_POST['ref_name'];
		$ref_cnum = $_POST['ref_cnum'];

			$scol_name = $_POST['scol_name'];
			$sql = "SELECT * FROM tbl_school WHERE scol_name=:scol_name";
			$stat = $conn->prepare($sql);
			$stat->execute(['scol_name' => $scol_name]);
			$data3 = $stat->fetch(PDO::FETCH_OBJ);
			$scol_id = $data3->scol_id;

			$comp_name = $_POST['comp_name'];
			$sql = "SELECT * FROM tbl_company WHERE comp_name=:comp_name";
			$stat = $conn->prepare($sql);
			$stat->execute(['comp_name' => $comp_name]);
			$data4 = $stat->fetch(PDO::FETCH_OBJ);
			$comp_id = $data4->comp_id;

		$imgFile = $_FILES['profile']['name'];
		$tmp_dir = $_FILES['profile']['tmp_name'];
		$imgSize = $_FILES['profile']['size'];
			if(empty($lname||$fname)){
				$errMSG = "Please Enter Your Full Name.";
			}
			else if(empty($cnum||$email)){
				$errMSG = "Please Enter Your Complete Contact.";
			}
			else if(empty($age || $address || $course || $skills || $obj || $ref_name || $ref_cnum || $scol_id || $comp_id)){
				$errMSG = "Please Fill Up Everything";
			}
			else if(empty($imgFile)){
				$errMSG = "Please Select Image File.";
			}
			else
			{
				$upload_dir = 'uploads/';
				$imgExt = strtolower(pathinfo($imgFile,PATHINFO_EXTENSION));
				$valid_extensions = array('jpeg', 'jpg', 'png', 'gif');
				$profile = rand(1000,1000000).".".$imgExt;
				if(in_array($imgExt, $valid_extensions)){
					if($imgSize < 5000000){
						move_uploaded_file($tmp_dir,$upload_dir.$profile);
					}
					else{
						$errMSG = "Sorry, Your File Is Too Large To Upload. It Should Be Less Than 5MB.";
						echo "<br>" . $errMSG;
					}
				}
				else{
					$errMSG = "Sorry, only JPG, JPEG, PNG & GIF Extension Files Are Allowed.";
					echo "<br>" . $errMSG;		
				}
			

			if(!isset($errMSG))
			{
			
			$sql = 'INSERT INTO tbl_student(last_name, first_name, mi_name, age, stud_address, course, scol_id, skills, stud_contact, stud_email, stud_obj, ref_name, ref_contact, comp_id, stud_profile)VALUES(:lname,:fname,:mname,:age,:address,:course,:scol_id,:skills,:cnum,:email,:obj,:ref_name,:ref_cnum,:comp_id,:profile)';
		    $stat = $conn->prepare($sql);
		    $stat->execute([':lname' => $lname,
				    		':fname' => $fname,
				    		':mname' => $mname,
				    		':age' => $age,
				    		':address' => $address,
				    		':course' => $course,
				    		':scol_id' => $scol_id,
				    		':skills' => $skills,
				    		':cnum' => $cnum,
				    		':email' => $email,
				    		':obj' => $obj,
				    		':ref_name' => $ref_name,
				    		':ref_cnum' => $ref_cnum,
				    		':comp_id' => $comp_id,
				    		':profile' => $profile]);

		   		header("Location: stud_data.php");
		   	}
		}
		echo "Nothing Happens?";
		echo "Well . . . ".$errMSG;
		}
?>